import React, { Component } from 'react';
import TrelloList from '../Components/TrelloList';
import Form from '../Components/Form';

const API_KEY = '82ea82d282e80349d1002f0fb1aa2cc7';
const TOKEN =
'fd325e0dbd53939043799abfb17713916d11e453b9a3de07688a87b484a47723';

const Styles = {
  listStyle: {
    display: 'flex',
    flexDirection: 'row'
  }
};

class Lists extends Component {
  constructor() {
    super();
    this.state = {
      lists: [],
      addListInput: '',
      open: false,
      cards: []
    };
  }

  getInputValue = e => {
    this.setState({
      addListInput: e
    });
  };
  
  componentDidMount() {
    const boardID = this.props.match.params.id;
    fetch(
      `https://api.trello.com/1/boards/${boardID}/lists?key=${API_KEY}&token=${TOKEN}`,
      {
        method: 'GET'
      }
    )
      .then(response => response.json())
      .then(data => {
        this.setState({
          lists: data
        });
      });
  }

  handleDeleteList = id => {
    fetch(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${API_KEY}&token=${TOKEN}`,
      {
        method: 'PUT'
      }
    ).then(() =>
      this.setState({
        lists: this.state.lists.filter(list => list.id !== id)
      })
    );
  };

  handleAddList = () => {
    
    const listName = this.state.addListInput;
    const listId = this.props.match.params.id;
    if (listName === '') {
      alert('can not add empty list');
    } else {
      fetch(
        `https://api.trello.com/1/lists?name=${listName}&idBoard=${listId}&pos=bottom&key=${API_KEY}&token=${TOKEN}`,
        {
          method: 'POST'
        }
      )
        .then(Response => Response.json())

        .then(newList =>
          this.setState({
            lists: this.state.lists.concat([newList]),

          
          })
        );
    }
  };

  render() {
    const lists = this.state.lists;

    return (
      <div style={Styles.listStyle}>
        <>
          {lists.map(list => (
            <TrelloList
              key={list.id}
              id={list.id}
              name={list.name}
              onDelete={this.handleDeleteList}
            />
          ))}
          <Form
            placeholder="Add a list"
            buttonName="Add List"
            onAdd={this.handleAddList}
            getInputValue={this.getInputValue}
          />
        </>
      </div>
    );
  }
}
export default Lists;
