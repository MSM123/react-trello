import React, { Component } from 'react';
import TrelloCards from '../Components/TrelloCards';
import Modal from '../Components/Modal';
import Form from '../Components/Form';

const API_KEY = '82ea82d282e80349d1002f0fb1aa2cc7';
const TOKEN =
'fd325e0dbd53939043799abfb17713916d11e453b9a3de07688a87b484a47723';

let cardId;
let card_name;
class Cards extends Component {
  constructor() {
    super();
    this.state = {
      cards: [],
      open: false,
      addCardInput: ''
    };
  }

  getInputValue = e => {
    this.setState({
      addCardInput: e
    });
  };

  deleteCard = id => {
    let allCards = this.state.cards;
    fetch(
      `https://api.trello.com/1/cards/${id}?key=${API_KEY}&token=${TOKEN}`,
      {
        method: 'DELETE'
      }
    ).then(() => {
      let CardsafterDelete = allCards.filter(card => card.id !== id);
      this.setState({
        cards: CardsafterDelete,
        
      });
    });
  };

  handleAdd = () => {
    const cardName = this.state.addCardInput;
    if (cardName !== '') {
      fetch(
        `https://api.trello.com/1/cards?name=${cardName}&idList=${this.props.id}&keepFromSource=all&key=${API_KEY}&token=${TOKEN}`,
        {
          method: 'POST'
        }
      )
        .then(cardData => cardData.json())
        .then(newCard =>
          this.setState({ cards: this.state.cards.concat([newCard]) })
        );
    } else {
      alert('can not add empty card');
    }
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  getID = props => {
    cardId = props.id;
    card_name = props.name;
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/lists/${this.props.id}/cards?key=${API_KEY}&token=${TOKEN}`,
      {
        method: 'GET'
      }
    )
      .then(data => data.json())
      .then(data => {
        this.setState({
          cards: data
        });
      });
  }

  render() {
    const cardsData = this.state.cards;

    return (
      <>
        <>
          {cardsData.map(card => (
            <TrelloCards
              name={card.name}
              id={card.id}
              key={card.id}
              onDelete={this.deleteCard}
              getCheckList={this.getID}
              onOpenModal={this.onOpenModal}
            />
          ))}
          <Form
            placeholder="add a card"
            buttonName="Add Card"
            onAdd={this.handleAdd}
            getInputValue={this.getInputValue}
          />
        </>
        <Modal
          open={this.state.open}
          closeModal={this.onCloseModal}
          cardId={cardId}
          card_name={card_name}
        />
      </>
    );
  }
}

export default Cards;
