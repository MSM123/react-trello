import React, { Component } from 'react';
import Board from '../Components/TrelloBoard';


const API_KEY = '82ea82d282e80349d1002f0fb1aa2cc7';
const TOKEN =
'fd325e0dbd53939043799abfb17713916d11e453b9a3de07688a87b484a47723';

class Boards extends Component {
  constructor() {
    super();
    this.state = {
      boards: []
    };
  }
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/mssingh1/boards?key=${API_KEY}&token=${TOKEN}`,
      {
        method: 'GET'
      }
    )
      .then(response => response.json())
      .then(data => {
        this.setState({
          boards: data
        });
      });
  }

  render() {
    const Data = this.state.boards;
    return Data.map(data => (
      <Board key={data.id} id={data.id} name={data.name} />
    ));
  }
}

export default Boards;



