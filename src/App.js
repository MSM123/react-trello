import React, { Component } from 'react';
import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom';
import Boards from './data/Boards';
import Lists from './data/Lists';
import Main from './Components/Main'


class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
  <Main />
    <Switch>
          <Route exact path="/boards" component={Boards} />
          <Route exact path="/boards/:id" component={Lists} />
          </Switch>
      </div>
      </Router>
    );
  }
}

export default App;
